#include <iostream>
using namespace std;

const int N = 8;

int a [N];
int cnt = 0;

void print_arr()
{
    //return;
    cout << "reseni na: ";
    for (int i = 0; i < N; i++)
        cout << a[i] << ", ";
    cout << "\n";
    cnt++;
}

void play (int k)
{

    // umistovat damu na k-ty sloupec
    for (int i = 0; i < N; i++)
    {
        bool ok = true;

        for (int v = 1; v <= k; v++)
        {
            int h = a[k - v];
            if (h == i || h == i + v || h == i - v)
                ok = false;
        }
        

        if (ok)
        {
            a[k] = i;

            if (k < N - 1)
                play(k + 1);
            else
                print_arr();
        }
    }
}

int main()
{
    play (0);

    cout << "O.K." << cnt << endl;
}

