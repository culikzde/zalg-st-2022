#include <iostream>
#include <cassert>
#include <time.h>
using namespace std;

const int N = 1000;  // pocet cislic

const int LEN = 8;  // pocet cislic
const int CNT = 10; // pocet prihradek

struct Item
{
    char key [LEN + 1];
    Item* next = nullptr;
};

struct List
{
    Item* first = nullptr;
    Item* last = nullptr;
};

List input;

List table[CNT];

inline void add (List & list, Item * item)
{
    item->next = nullptr;
    if (list.first == nullptr)
    {
        list.first = item;
    }
    else
    {
        list.last->next = item;
    }
    list.last = item;
}

inline void join (List& target, List& source)
{
    if (source.first != nullptr)
    {
        if (target.first == nullptr)
            target.first = source.first;
        else 
            target.last->next = source.first;

        target.last = source.last;
    }
}

inline void clear(List& target)
{
    target.first = nullptr;
    target.last = nullptr;
}

void print()
{
    Item* u = input.first;
    while (u != nullptr)
    {
        cout << u->key << endl;
        u = u->next;
    }
}

void sort (int t)
{
    for (int inx = 0; inx < CNT; inx++)
       clear (table [inx]);

    Item * u = input.first;
    while (u != nullptr)
    {
        Item* nxt = u->next;

        int inx = u->key[t] - '0';
        // cout << u->key << " ..." << inx << endl;
        assert(inx >= 0 && inx < CNT);
        add (table[inx], u);

        u = nxt;
    }

    clear (input);
    for (int inx = 0; inx < CNT; inx++)
        join (input, table[inx]);

    /*
    cout << endl;
    cout << "sort " << t << endl;
    print ();
    */

    cout << endl;
}

void sort()
{
    for (int t = LEN-1; t >= 0; t--)
         sort (t);
}

void enter (int v)
{
    Item* item = new Item;

    item->key[LEN] = '\0';
    for (int t = LEN - 1; t >= 0; t--)
    {
        int d = v % 10;
        v = v / 10;
        item->key[t] = d + '0';
    }

    add (input, item);
}

int main ()
{
    clear(input);

    srand(time(nullptr));
    for (int i = 0; i < N; i++)
        enter (rand() % 10000 + 10000 * (rand() % 10000));

    /*
    enter(875);
    enter(102);
    enter(1875);
    enter(302);
    */

    sort();
    print();
    cout << "O.K." << endl;
}

