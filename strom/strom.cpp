#include <iostream>
using namespace std;

class Item
{
public:
    int key;

    Item* left;
    Item* right;

    Item() : key (0), left(nullptr), right(nullptr) { }
};


//       5
//   3      8

Item* find (Item* from, int looking_for)
{

    if (from == nullptr)
        return nullptr;

    if (from->key == looking_for)
        return from;

    if (from->key > looking_for)
        return find(from->left, looking_for);
    else
        return find(from->right, looking_for);
}

void insert (Item * & from, int fresh)
{
    if (from == nullptr)
    {
        from = new Item;
        from->key = fresh;
        from->left = nullptr;
        from->right = nullptr;
    }
    else if (fresh == from->key)
    {
        cout << "item is found in tree" << endl;
    }
    else if (from->key < fresh)
    {
        insert (from->right, fresh);
    }
    else
    {
        insert (from->left, fresh);
    }
}


Item* search(Item* from, int looking_for)
{
    while (from != nullptr && from->key != looking_for) {
       if (looking_for < from->key)
            from = from->left;
        else
            from = from->right;
    }
    return from;
}

Item* release (Item * & branch)
{
    Item* t = branch;
    if (branch->right != nullptr)
    {
        t = release(branch->right);
    }
    else
    {
        branch = branch->left;
    }
    t->left = nullptr;
    return t;
}

void remove(Item*& from, int old)
{
    if (from == nullptr)
    {
    }
    else if (old < from->key)
    {
        remove (from->left, old);
    }
    else if (old > from->key)
    {
        remove (from->right, old);
    }
    else /* if (from->key == old) */
    {
        Item* t = from;
        if (from->left == nullptr )
        {
            from = from->right;
        }
        else if (from->right == nullptr)
        {
            from = from->left;
        }
        else
        {
            Item * n = release (from->left);
            n->left = from->left;
            n->right = from->right;
            from = n;
        }
        delete t;
    }
}




/*
void swap(int& a, int& b)
{
    cout << "a=" << a << ", b=" << b << endl;
    int t = a;
    a = b;
    b = t;
    cout << "a=" << a << ", b=" << b << endl;
}

void swap2 (int * a, int * b)
{
    cout << "a=" << *a << ", b=" << *b << endl;
    int t = *a;
    *a = *b;
    *b = t;
    cout << "a=" << *a << ", b=" << *b << endl;
}
*/

void print (Item* from, int level = 0)
{
    if (from != nullptr)
    {
        print(from->left, level+1);
        for (int i = 0; i < level; i++)
            cout << "    ";
        cout << from->key << endl;
        print(from->right, level+1);
    }
}

Item* root = nullptr;

int main()
{
    insert(root, 5);
    insert(root, 3);
    insert(root, 8);
    insert(root, 7);

    while (root != nullptr)
       remove (root, root->key);

    Item* t = search(root, 5);
    if (t == nullptr)
        cout << "nenasli" << endl;
    else
        cout << "nasli " << t->key<< endl;

    print(root);

    /*
    int x = 10;
    int y = 20;
    cout << "x=" << x << ", y=" << y << endl;
    swap (x, y);
    cout << "x=" << x << ", y=" << y << endl;
    */

    cout << "O.K." << endl;
}
