#include <iostream>
using namespace std;

const int N = 8;

int p[N][N];
int zr[N][N]; // zr[r][s] ...  puvodni radka rr
int zs[N][N]; // zr[r][s] ...  puvodni sloupec ss
 

void skoc(int r, int s, int v, int rr, int ss)
{
    if (r >= 0 && r < N && s >= 0 && s < N)
    {
        if (p[r][s] == -1 || p[r][s] > v)
        {
            p[r][s] = v;
            zr[r][s] = rr;
            zs[r][s] = ss;

            skoc (r + 1, s + 2, v + 1, r, s);
            skoc (r - 1, s + 2, v + 1, r, s);
            skoc (r + 1, s - 2, v + 1, r, s);
            skoc (r - 1, s - 2, v + 1, r, s);

            skoc (r + 2, s + 1, v + 1, r, s);
            skoc (r - 2, s + 1, v + 1, r, s);
            skoc (r + 2, s - 1, v + 1, r, s);
            skoc (r - 2, s - 1, v + 1, r, s);
        }
    }
}


void cesta (int r, int s)
{
    if (r >= 0 && s >= 0)
    {
        cesta (zr[r][s], zs[r][s]);
        cout << r << ", " << s << endl;
    }
}

int main()
{
    for (int i = 0; i < N; i++)
        for (int k = 0; k < N; k++)
        {
            p[i][k] = -1; // nenavstivena policka
            zr[i][k] = -1; // nenavstivena policka
            zs[i][k] = -1; // nenavstivena policka
        }

    int r = 0;
    int s = 0;
    skoc(r, s, 0, -1, -1);

    for (int i = 0; i < N; i++)
    {
        for (int k = 0; k < N; k++)
        {
            int v = p[i][k];
            if (v == -1)
                cout << ".";
            else if (v == -2)
                cout << "X";
            else
                cout << v;
            cout << " ";
        }
        cout << endl;
    }

    cesta (7, 7);

    /*
    r = 7;
    s = 7;

    while (r >= 0 && s >= 0)
    {
        cout << r << ", " << s << endl;
        int rr = zr[r][s];
        s = zs[r][s];
        r = rr;
    }
    */

    cout << "O.K." << endl;
}

