#include <iostream>
#include <time.h>
using namespace std;


const int N = 10000000;
int a[N] = { 2, 10, 22, 14, 22, 5, 10, 10, 10, 8, 1, 15, 10, 17 };


void quicksort(int l = 0, int r = N - 1)
{
    if (l < r)
    {
        //partition
        int pivot = a[(l + r) / 2];

        int l_i = l;
        int r_i = r;
        while (true)
        {
            while (a[l_i] < pivot) l_i++;
            while (a[r_i] > pivot) r_i--;

            if (l_i > r_i)
                break;

            std::swap(a[l_i], a[r_i]);
            l_i++;
            r_i--;
        }

        //recursive call
        quicksort(l_i, r);
        quicksort(l, r_i);
    }
}

int main()
{
    if (true)
    {
        srand(time(nullptr));
        for (int i = 0; i < N; i++)
            a[i] = rand() % 10000;
    }

    quicksort();

    bool ok = true;
    for (int i = 0; i < N; i++)
    {
        if (N <= 1000) cout << a[i];
        if (i < N - 1 && a[i + 1] < a[i]) { ok = false; cout << " CHYBA"; }
        if (N <= 1000) cout << endl;
    }

    if (ok)
        cout << "O.K." << endl;
}