#include <cassert>
#include <iostream>
using namespace std;

class Item
{
public:
    string name;
    int r, g, b;

    Item * above;
    Item * prev;
    Item * next;
    Item * first;
    Item * last;

    void add_prev(Item* fresh);
    void add_next(Item* fresh);

    void add_first(Item* fresh);
    void add_last(Item* fresh);

    void remove ();

    void print(int level = 0);
    Item (string name0 = "", int r0 = 255, int g0 = 255, int b0 = 255);
    ~ Item();

private:
    void indent(int level);
    void link (Item* before, Item* fresh, Item* after);
    void unlink();
};

Item::Item (string name0, int r0, int g0, int b0) :
    name (name0),
    r (r0),
    g (g0),
    b (b0),
    above (nullptr),
    prev(nullptr),
    next(nullptr),
    first(nullptr),
    last(nullptr)
{
}

void Item::link(Item* before, Item* fresh, Item* after)
{
    assert(fresh != nullptr);
    assert(fresh->above == nullptr);
    fresh->above = this;

    fresh->prev = before;
    fresh->next = after;

    if (before != nullptr)
        before->next = fresh;
    else
        first = fresh;

    if (after != nullptr)
        after->prev = fresh;
    else
        last = fresh;
}

void Item::unlink ()
{
    assert (above == nullptr);

    if (prev == nullptr)
        above->first = next;
    else
        prev->next = next;

    if (next == nullptr)
        above->last = prev;
    else
        next->prev = prev;

    above = nullptr;
    prev = nullptr;
    next = nullptr;
}

void Item::remove()
{
    if (above != nullptr)
       unlink ();

    Item* t = first;
    while (t != nullptr)
    {
        Item* p = t->next;
        t->unlink();
        delete t;
        t = p;
    }
}

Item::~Item()
{
    cout << "erasing " << name << endl;

    if (above != nullptr)
        unlink();

    Item* t = first;
    while (t != nullptr)
    {
        Item* p = t->next;
        t->above = nullptr;
        t->prev = nullptr;
        t->next = nullptr;
        delete t;
        t = p;
    }
}
    
void Item::add_first(Item* fresh)
{
    link (nullptr, fresh, first);
}


void Item::add_last(Item* fresh)
{
    link (last, fresh, nullptr);
}

void Item::add_prev(Item* fresh)
{
    assert (above != nullptr);
    above->link(prev, fresh, this);
}

void Item::add_next(Item* fresh)
{
    assert(above != nullptr);
    above->link(this, fresh, this->next);
}

void Item::indent(int level)
{
    for (int i = 1; i <= 4 * level; i++)
        cout << ' ';
}

void Item::print(int level)
{
    if (name != "")
    {
        indent(level);
        cout << "Jmeno: " << name << " r: " << r << " g: " << g << " b: " << b << endl;
    }

    Item* a = first;
    if (a != nullptr)
    {
        indent(level);
        cout << "{" << endl;
        while (a != nullptr)
        {
            a->print (level + 1);
            a = a->next;
        }
        indent(level);
        cout << "}" << endl;
    }
}

int main()
{
    Item a ("barvy");

    a.add_last(new Item("cervena", 255, 0, 0));
    Item* c = a.first;
    a.add_first(new Item("modra", 0, 0, 255));
    a.add_first(new Item("zelena", 0, 255, 0 ));
    a.add_last(new Item("zluta", 255, 255, 0 ));

    a.first->add_next(new Item("oranzova", 255, 128, 0));
    a.last->add_prev(new Item("fialova", 255, 0, 255));

    c->add_first(new Item("svetle cervena", 255, 128, 128));
    c->add_last(new Item("jina cervena", 255, 192, 192));

    a.print();

    cout << "O.K." << endl;
}

