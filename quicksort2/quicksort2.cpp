#include <iostream>
#include <time.h>
using namespace std;

const int N = 1000000;
int a[N]; // = { 20, 30, 40, 7, 5, 80, 22, 33, 10, 24 };

void swap (int& x, int& y)
{
    int t = x;
    x = y;
    y = t;
}

void quicksort (int r, int s)
{
    int i = r;
    int k = s;
    int h = a [(r + s) / 2];

    while (i <= k)
    {
        while (a[i] < h) i++;
        while (a[k] > h) k--;

        if (i <= k)
        {
            swap(a[i], a[k]);
            i++;
            k--;
        }
    }

    if (r<k) quicksort (r, k);
    if (i<s) quicksort (i, s);
}

int main()
{
    srand (time (nullptr));
    for (int i = 0; i < N; i++)
        a[i] = rand() % 10000 + 10000 * (rand() % 10000);


    quicksort (0, N-1);

    bool ok = true;
    for (int i = 0; i < N; i++)
    {
        if (i < N-1)
            if (a[i + 1] < a[i])
            {
                ok = false;
                cout << "CHYBA ";
            }
        // cout << a[i] << endl;
    }

    if (ok)
       cout << "O.K" << endl;
}

