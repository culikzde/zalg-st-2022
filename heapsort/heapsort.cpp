#include <iostream>
#include <time.h>
using namespace std;

const int N = 1000000;
int a[N] /* = { 2, 7, 8, 15, 20, 3, 10, 12, 1, 5 } */;

inline int left(int i)
{
    return 2 * i + 1;
}

inline int right(int i)
{
    return 2 * i + 2;
}

void fall(int i, int heap_end)
{
    for (;;) {
        bool has_left = left(i) <= heap_end;
        bool has_right = right(i) <= heap_end;
        if (!has_left && !has_right) break;
        else if (!has_right) {
            if (a[left(i)] > a[i]) {
                swap(a[i], a[left(i)]);
                i = left(i);
            }
            else {
                break;
            }
        }
        else {
            int to_swap;
            if (a[left(i)] > a[right(i)]) {
                to_swap = left(i);
            }
            else {
                to_swap = right(i);
            }
            if (a[to_swap] > a[i]) {
                swap(a[i], a[to_swap]);
                i = to_swap;
            }
            else {
                break;
            }
        }
    }

}

void heapSort()
{
    for (int i = N/2; i >= 0; --i) {
        fall(i, N - 1);
    }
    for (int i = N - 1; i >= 1; --i) {
        swap(a[0], a[i]);
        fall(0, i - 1);
    }
}


int main()
{
    srand(time(nullptr));
    for (int i = 0; i < N; i++)
        a[i] = rand() % 10000 + 10000* (rand() % 10000);

    heapSort ();

    bool ok = true;
    for (int i = 0; i < N; ++i) {
        if (i < N - 1 && a[i] > a[i + 1])
        {
            ok = false;
            cout << " ERR ";
        }
        // cout << a[i] << " ";
    }

    if (ok)
       cout << "O.K." << endl;
}
